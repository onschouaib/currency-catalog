# Technology
- Angular: 7.2.6
- Angular CLI: 7.3.3
- Node: 11.5.0
- OS: win32 x64
- ng-bootstrap

# Afficher liste currency-catalog
- Afficher l'ID de chaque devise;
- Afficher le Type des devises;

# Afficher Detail currency-catalog
- Afficher les détails de chaque devise : 
    code, 
    name, 
    currency_type, 
    code_iso_numeric3, 
    code_iso_alpha3;

# Bonus currency-catalog
- Pagination sur la liste des devises;
- Filtré la liste des devises selon : 
    ID, 
    Code, 
    Name, 
    Type;
- Les interfaces utilisateurs responsive
