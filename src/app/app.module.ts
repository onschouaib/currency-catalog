import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CurrencyListComponent } from './composants/currency-list/currency-list.component';
import { CurrencyComponent } from './composants/currency-detail/currency-detail.component';

import { FormsModule } from '@angular/forms';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


import { HttpClientModule }    from '@angular/common/http';
import { SearchComponent } from './composants/utils/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrencyListComponent,
    CurrencyComponent,
    SearchComponent,
  ],
  imports: [
    BrowserModule, 

    HttpClientModule,
    FormsModule,
    NgbPaginationModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
