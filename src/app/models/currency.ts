import {Attribute} from './currencyAttribute';

export class Currency {
  public id: string;
  public attributes: Attribute;
}
