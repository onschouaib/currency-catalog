import {Currency} from './currency';

export class CurrencyList {
  public total: number;
  public pageSize: number;
  public currencies: Currency[];
}
