import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrencyListComponent } from './composants/currency-list/currency-list.component';
import { CurrencyComponent } from './composants/currency-detail/currency-detail.component';

const routes: Routes = [
  {path: 'currency-list', component: CurrencyListComponent},
  {path: 'currency/:id', component: CurrencyComponent},
  {path: '', redirectTo: '/currency-list', pathMatch: 'full'},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
