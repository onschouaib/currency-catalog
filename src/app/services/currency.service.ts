import { Injectable } from '@angular/core';
import {CurrencyList} from '../models/currencyList';
import {CurrenciesMock} from '../models/currencies-mock';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Currency } from '../models/currency';
@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(private http:HttpClient) {
  }

  private currencylist: CurrencyList = {
    pageSize: 10,
    total:  CurrenciesMock.currencies.length,
    currencies : CurrenciesMock.currencies
  }
  

  public getAllCurrenies(page:number, pageSize: number, search:string,filter:string): Observable<any> {
    
    this.currencylist.currencies = CurrenciesMock.currencies;
    this.currencylist.pageSize = pageSize;
    this.currencylist.total = CurrenciesMock.currencies.length;
    
    if(filter != "" && search != ""){
        if(filter == 'id'){
          let currencies = this.currencylist.currencies.filter(currency => currency.id.toLowerCase().includes(search.toLowerCase()));
          this.currencylist.currencies = currencies;
          this.currencylist.total = currencies.length;
          
        }else{
          let currencies = this.currencylist.currencies.filter(obj => {
            return obj.attributes[filter].toLowerCase().includes(search.toLowerCase())
          })
          this.currencylist.currencies = currencies;
          this.currencylist.total = currencies.length;
        }
    }



    this.currencylist.pageSize = pageSize;
    let start = (page > 1) ? ((page-1)*pageSize) : 0;
    let end = start + pageSize;
    this.currencylist.currencies = this.currencylist.currencies.slice(start, end);
    
    const currencyObservable = new Observable<any>(observer => {
           setTimeout(() => {
               observer.next(this.currencylist);
           }, 1000);
    });
    return currencyObservable; 
  }

  

  public getCurrencyById(id: string): Observable<Currency> {
    const currencies = this.currencylist.currencies.filter(currency => currency.id.toLowerCase() === id.toLowerCase());
    if (currencies.length) {
      return of(currencies[0]);
    }
    return of(null);
  }
}
