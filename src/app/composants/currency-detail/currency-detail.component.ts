import { Component, OnInit } from '@angular/core';
import {Currency} from "../../models/currency";
import {CurrencyService} from "../../services/currency.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-currency',
  templateUrl: './currency-detail.component.html',
  styleUrls: ['./currency-detail.component.css']
})
export class CurrencyComponent implements OnInit {

  public currency: Currency;

  constructor(private currencyService : CurrencyService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.currencyService.getCurrencyById(this.route.snapshot.params['id'])
    .subscribe( currency => {
      if (currency === null) {
        this.router.navigate(['/']);
      } else {
        this.currency = currency;
      }
    } );
  }

}
