import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Input() searchInput: string;
  @Input() filter: string;
  @Output() onChangeSearch = new EventEmitter<any>(); 
  @Output() onChangeFilter = new EventEmitter<any>(); 

  constructor() { }

  ngOnInit() {
  }

  onSearch() {
    this.onChangeSearch.emit(this.searchInput);  
  }

  onFilter() {
    this.onChangeFilter.emit(this.filter);  
  }
}
