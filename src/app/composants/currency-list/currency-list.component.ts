import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CurrencyService } from '../../services/currency.service';
import { Currency } from '../../models/currency';
import { Attribute } from '../../models/currencyAttribute';
import { CurrencyList } from 'src/app/models/currencyList';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.css']
})
export class CurrencyListComponent implements OnInit {

  currencies: Currency[];
  id: string ;
  currentPage: number;
  pageSize: number;
  total:number;
  searchInput: string;
  filter:string;

  constructor(private currencyService: CurrencyService) {
    this.currentPage = 1;
    this.pageSize = 10;
    this.searchInput = "";
    this.filter = "";
  }

  ngOnInit() 
    {
      const currenciOb= this.currencyService.getAllCurrenies(this.currentPage, this.pageSize, this.searchInput, this.filter);
      currenciOb.subscribe((currencyList: CurrencyList) => 
      {
        this.currencies = currencyList.currencies;
        this.total = currencyList.total;
      });
        
  } 

  search(){    
    const currenciOb= this.currencyService.getAllCurrenies(this.currentPage, this.pageSize, this.searchInput, this.filter);
      currenciOb.subscribe((currencyList: CurrencyList) => 
      {
        this.currencies = currencyList.currencies;
        this.total = currencyList.total;
      });

  }

  onChangeSize()
  {
    this.currentPage = 1;
    const currenciOb= this.currencyService.getAllCurrenies(this.currentPage, this.pageSize, this.searchInput, this.filter);
      currenciOb.subscribe((currencyList: CurrencyList) => 
      {
        this.currencies = currencyList.currencies;
        this.total = currencyList.total;
      });
  }

  setFilter(filter){
    this.filter = filter;
    this.search();
  }

  setSearch(search){
    this.searchInput = search;
    this.search();
  }
  

  
}
